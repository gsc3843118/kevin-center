import { Helmet } from 'react-helmet-async';

// @mui
import { Container, Grid } from '@mui/material';
// components
import { useSettingsContext } from '../../components/settings';

import { useAuthContext } from '../../auth/useAuthContext';
import { AppTopAuthors } from '../../sections/@dashboard/general/app';

// ----------------------------------------------------------------------

export default function UserPage() {
  const { themeStretch } = useSettingsContext();
  const { user } = useAuthContext();

  return (
    <>
      <Helmet>
        <title> User Page | Minimal UI</title>
      </Helmet>

      <Container maxWidth={themeStretch ? false : 'xl'}>
        <Grid item xs={12} md={6} lg={4}>
          <AppTopAuthors title="Account Information" list={[user]} />
        </Grid>
      </Container>
    </>
  );
}
