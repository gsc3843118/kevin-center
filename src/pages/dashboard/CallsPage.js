import { Helmet } from 'react-helmet-async';
import { useEffect, useState } from 'react';

// @mui
import {
  Card,
  Table,
  Button,
  Tooltip,
  TableBody,
  Container,
  IconButton,
  TableContainer,
  Typography,
} from '@mui/material';
// components
import Iconify from '../../components/iconify';
import Scrollbar from '../../components/scrollbar';
import ConfirmDialog from '../../components/confirm-dialog';
import {
  useTable,
  emptyRows,
  TableEmptyRows,
  TableHeadCustom,
  TableSelectedAction,
  TablePaginationCustom,
} from '../../components/table';
// sections
import { UserTableRow } from '../../sections/@dashboard/user/list';
// components
import { useSettingsContext } from '../../components/settings';

import { useAuthContext } from '../../auth/useAuthContext';
import Page403 from '../Page403';

const TABLE_HEAD = [
  { id: 'phone', label: 'Phone', align: 'left' },
  { id: 'time', label: 'Time', align: 'left' },
  { id: 'status', label: 'Status', align: 'left' },
  { id: 'DTMF', label: 'DTMF', align: 'center' },
  { id: '' },
];

// ----------------------------------------------------------------------

export default function CallsPage() {
  const {
    dense,
    page,
    order,
    orderBy,
    rowsPerPage,
    setPage,
    selected,
    setSelected,
    onSelectRow,
    onSelectAllRows,
    onSort,
    onChangeDense,
    onChangePage,
    onChangeRowsPerPage,
  } = useTable();

  const { themeStretch } = useSettingsContext();
  const { user, calls, deleteCall, forwardCall } = useAuthContext();
  const [callsList, setCallsList] = useState([]);
  const key = localStorage.getItem('accessToken');

  const [openConfirm, setOpenConfirm] = useState(false);

  const dataInPage = callsList.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage);

  const denseHeight = dense ? 52 : 72;

  const handleOpenConfirm = () => {
    setOpenConfirm(true);
  };

  const handleCloseConfirm = () => {
    setOpenConfirm(false);
  };

  const handleDeleteRow = async (id, phone) => {
    await deleteCall(phone);
    const deleteRow = callsList.filter((row) => row.phone !== phone);
    setSelected([]);
    setCallsList(deleteRow);
    await calls(key);

    if (page > 0) {
      if (dataInPage.length < 2) {
        setPage(page - 1);
      }
    }
  };

  const handleDeleteRows = async (selectedRows) => {
    selectedRows.forEach(async (row) => {
      await deleteCall(row);
    });
    const deleteRows = callsList.filter((row) => !selectedRows.includes(row.id));
    setSelected([]);
    await calls(key);

    if (page > 0) {
      if (selectedRows.length === dataInPage.length) {
        setPage(page - 1);
      } else if (selectedRows.length === callsList.length) {
        setPage(0);
      } else if (selectedRows.length > dataInPage.length) {
        const newPage = Math.ceil((callsList.length - selectedRows.length) / rowsPerPage) - 1;
        setPage(newPage);
      }
    }
  };

  useEffect(() => {
    const getCalls = async () => {
      let timer;
      clearInterval(timer);

      timer = setInterval(async () => {
        if (user.role === 'verified' || user.role === 'admin') {
          const data = await calls(key);
          setCallsList(
            data.filter((call) => call.status === user.email || call.status === '0').reverse()
          );
        }
        // setCallsList(data.reverse());
      }, 3000);
      return () => {
        clearInterval(timer);
      };
    };

    getCalls();
  }, [calls, key, user.email, user.role]);

  return (
    <>
      <Helmet>
        <title> Calls: List | Centre</title>
      </Helmet>

      {user.role !== 'Unverified' && (
        <Container maxWidth={themeStretch ? false : 'lg'}>
          {callsList && (
            <Card>
              <TableContainer sx={{ position: 'relative', overflow: 'unset' }}>
                <TableSelectedAction
                  dense={dense}
                  numSelected={selected.length}
                  rowCount={callsList.length}
                  onSelectAllRows={(checked) =>
                    onSelectAllRows(
                      checked,
                      callsList.map((row) => row.id)
                    )
                  }
                  action={
                    <Tooltip title="Delete">
                      <IconButton color="primary" onClick={handleOpenConfirm}>
                        <Iconify icon="eva:trash-2-outline" />
                      </IconButton>
                    </Tooltip>
                  }
                />

                <Scrollbar>
                  <Table size={dense ? 'small' : 'medium'} sx={{ minWidth: 800 }}>
                    <TableHeadCustom
                      order={order}
                      orderBy={orderBy}
                      headLabel={TABLE_HEAD}
                      rowCount={callsList.length}
                      numSelected={selected.length}
                      onSort={onSort}
                      onSelectAllRows={(checked) =>
                        onSelectAllRows(
                          checked,
                          callsList.map((row) => row.id)
                        )
                      }
                    />

                    <TableBody>
                      {callsList
                        .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                        .map((row) => (
                          <UserTableRow
                            key={row.id}
                            row={row}
                            selected={selected.includes(row.phone)}
                            onSelectRow={() => onSelectRow(row.phone)}
                            onDeleteRow={() => handleDeleteRow(row.id, row.phone)}
                            forwardCall={() => forwardCall(row.phone, key)}
                          />
                        ))}

                      <TableEmptyRows
                        height={denseHeight}
                        emptyRows={emptyRows(page, rowsPerPage, callsList.length)}
                      />
                    </TableBody>
                  </Table>
                </Scrollbar>
              </TableContainer>

              <TablePaginationCustom
                count={callsList.length}
                page={page}
                rowsPerPage={rowsPerPage}
                onPageChange={onChangePage}
                onRowsPerPageChange={onChangeRowsPerPage}
                //
                dense={dense}
                onChangeDense={onChangeDense}
              />
            </Card>
          )}
        </Container>
      )}
      {user.role === 'Unverified' && <Page403 />}

      <ConfirmDialog
        open={openConfirm}
        onClose={handleCloseConfirm}
        title="Delete"
        content={
          <>
            Are you sure want to delete <strong> {selected.length} </strong> items?
          </>
        }
        action={
          <Button
            variant="contained"
            color="error"
            onClick={() => {
              handleDeleteRows(selected);
              handleCloseConfirm();
            }}
          >
            Delete
          </Button>
        }
      />
    </>
  );
}
