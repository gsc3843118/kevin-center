import { Helmet } from 'react-helmet-async';
import { useState, useEffect } from 'react';
// @mui
import {
  Card,
  Table,
  Button,
  TableBody,
  Container,
  TableContainer,
  Tooltip,
  IconButton,
} from '@mui/material';
// components
import Iconify from '../../components/iconify';
import Scrollbar from '../../components/scrollbar';
import ConfirmDialog from '../../components/confirm-dialog';
import { useSettingsContext } from '../../components/settings';
import {
  useTable,
  emptyRows,
  TableEmptyRows,
  TablePaginationCustom,
  TableSelectedAction,
  TableHeadCustom,
} from '../../components/table';
// sections
import { UserTableRow2 } from '../../sections/@dashboard/user/list';
import { useAuthContext } from '../../auth/useAuthContext';
import Page403 from '../Page403';

// ----------------------------------------------------------------------

const TABLE_HEAD = [
  { id: 'id', label: 'Id', align: 'left' },
  { id: 'username', label: 'Username', align: 'left' },
  { id: 'permission', label: 'Permission', align: 'left' },
  { id: 'Phone Number', label: 'Number', align: 'left' },
  { id: '' },
];

// ----------------------------------------------------------------------

export default function UserListPage() {
  const {
    dense,
    page,
    order,
    orderBy,
    rowsPerPage,
    setPage,
    //
    selected,
    setSelected,
    onSelectRow,
    onSelectAllRows,
    //
    onSort,
    onChangeDense,
    onChangePage,
    onChangeRowsPerPage,
  } = useTable();

  const { themeStretch } = useSettingsContext();

  const [openConfirm, setOpenConfirm] = useState(false);

  const [accountsList, setAccountsList] = useState('');
  const [isEdited, setIsEdited] = useState(false);

  const { user, accounts, deleteAccount, editAccount } = useAuthContext();
  const key = localStorage.getItem('accessToken');

  const dataInPage = accountsList.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage);

  const denseHeight = dense ? 52 : 72;

  const handleOpenConfirm = () => {
    setOpenConfirm(true);
  };

  const handleCloseConfirm = () => {
    setOpenConfirm(false);
  };

  const handleDeleteRow = async (id, phone) => {
    await deleteAccount(phone);
    const deleteRow = accountsList.filter((row) => row.phone !== phone);
    setSelected([]);
    setAccountsList(deleteRow);
    await accounts(key);

    if (page > 0) {
      if (dataInPage.length < 2) {
        setPage(page - 1);
      }
    }
  };

  const handleDeleteRows = async (selectedRows) => {
    selectedRows.forEach(async (row) => {
      await deleteAccount(row);
    });
    const deleteRows = accountsList.filter((row) => !selectedRows.includes(row.id));
    setSelected([]);
    setAccountsList(deleteRows);
    await accounts(key);

    if (page > 0) {
      if (selectedRows.length === dataInPage.length) {
        setPage(page - 1);
      } else if (selectedRows.length === accountsList.length) {
        setPage(0);
      } else if (selectedRows.length > dataInPage.length) {
        const newPage = Math.ceil((accountsList.length - selectedRows.length) / rowsPerPage) - 1;
        setPage(newPage);
      }
    }
  };

  const handleEditRow = (id) => {};

  useEffect(() => {
    const getCalls = async () => {
      const data = await accounts(key);
      setAccountsList(data);
      // setCallsList(data.reverse());
    };
    getCalls();
  }, [accounts, key]);

  return (
    <>
      <Helmet>
        <title> User: List | Minimal UI</title>
      </Helmet>

      {user.role === 'admin' && (
        <Container maxWidth={themeStretch ? false : 'lg'}>
          <Card>
            <TableContainer sx={{ position: 'relative', overflow: 'unset' }}>
              <TableSelectedAction
                dense={dense}
                numSelected={selected.length}
                rowCount={accountsList.length}
                onSelectAllRows={(checked) =>
                  onSelectAllRows(
                    checked,
                    accountsList.map((row) => row.id)
                  )
                }
                action={
                  <Tooltip title="Delete">
                    <IconButton color="primary" onClick={handleOpenConfirm}>
                      <Iconify icon="eva:trash-2-outline" />
                    </IconButton>
                  </Tooltip>
                }
              />
              {accountsList && (
                <Scrollbar>
                  <Table size={dense ? 'small' : 'medium'} sx={{ minWidth: 800 }}>
                    <TableHeadCustom
                      order={order}
                      orderBy={orderBy}
                      headLabel={TABLE_HEAD}
                      rowCount={accountsList.length}
                      numSelected={selected.length}
                      onSort={onSort}
                      onSelectAllRows={(checked) =>
                        onSelectAllRows(
                          checked,
                          accountsList.map((row) => row.id)
                        )
                      }
                    />
                    <TableBody>
                      {accountsList
                        .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                        .map((row) => (
                          <UserTableRow2
                            key={row.id}
                            row={row}
                            selected={selected.includes(row.id)}
                            onSelectRow={() => onSelectRow(row.id)}
                            onDeleteRow={() => handleDeleteRow(row.id)}
                            onEditRow={() => handleEditRow(row.name)}
                          />
                        ))}

                      <TableEmptyRows
                        height={denseHeight}
                        emptyRows={emptyRows(page, rowsPerPage, accountsList.length)}
                      />
                    </TableBody>
                  </Table>
                </Scrollbar>
              )}
            </TableContainer>

            <TablePaginationCustom
              count={accountsList.length}
              page={page}
              rowsPerPage={rowsPerPage}
              onPageChange={onChangePage}
              onRowsPerPageChange={onChangeRowsPerPage}
              //
              dense={dense}
              onChangeDense={onChangeDense}
            />
          </Card>
        </Container>
      )}

      {user.role !== 'admin' && <Page403 />}

      <ConfirmDialog
        open={openConfirm}
        onClose={handleCloseConfirm}
        title="Delete"
        content={
          <>
            Are you sure want to delete <strong> {selected.length} </strong> items?
          </>
        }
        action={
          <Button
            variant="contained"
            color="error"
            onClick={() => {
              handleDeleteRows(selected);
              handleCloseConfirm();
            }}
          >
            Delete
          </Button>
        }
      />
    </>
  );
}
