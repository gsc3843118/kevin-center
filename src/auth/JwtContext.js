import PropTypes from 'prop-types';
import { createContext, useEffect, useReducer, useCallback, useMemo } from 'react';
// utils
import axios from '../utils/axios';
import localStorageAvailable from '../utils/localStorageAvailable';
//
import { isValidToken, setSession } from './utils';

// ----------------------------------------------------------------------

// NOTE:
// We only build demo at basic level.
// Customer will need to do some extra handling yourself if you want to extend the logic and other features...

// ----------------------------------------------------------------------

const initialState = {
  isInitialized: false,
  isAuthenticated: false,
  user: null,
};

const reducer = (state, action) => {
  if (action.type === 'INITIAL') {
    return {
      isInitialized: true,
      isAuthenticated: action.payload.isAuthenticated,
      user: action.payload.user,
    };
  }
  if (action.type === 'LOGIN') {
    return {
      ...state,
      isAuthenticated: true,
      user: action.payload.user,
    };
  }
  if (action.type === 'REGISTER') {
    return {
      ...state,
      isAuthenticated: true,
      user: action.payload.user,
    };
  }
  if (action.type === 'LOGOUT') {
    return {
      ...state,
      isAuthenticated: false,
      user: null,
    };
  }

  return state;
};

// ----------------------------------------------------------------------

export const AuthContext = createContext(null);

// ----------------------------------------------------------------------

AuthProvider.propTypes = {
  children: PropTypes.node,
};

export function AuthProvider({ children }) {
  const [state, dispatch] = useReducer(reducer, initialState);

  const storageAvailable = localStorageAvailable();

  const initialize = useCallback(async () => {
    try {
      const accessToken = storageAvailable ? localStorage.getItem('accessToken') : '';

      if (accessToken && isValidToken(accessToken)) {
        const { username, phone, perms } = await setSession(accessToken);

        dispatch({
          type: 'INITIAL',

          payload: {
            isAuthenticated: true,
            user: {
              name: username,
              email: phone,
              // eslint-disable-next-line no-nested-ternary
              role: perms === 0 ? 'Unverified' : perms === 1 ? 'verified' : 'admin',
            },
          },
        });
      } else {
        dispatch({
          type: 'INITIAL',
          payload: {
            isAuthenticated: false,
            user: null,
          },
        });
      }
    } catch (error) {
      console.error(error);
      dispatch({
        type: 'INITIAL',
        payload: {
          isAuthenticated: false,
          user: null,
        },
      });
    }
  }, [storageAvailable]);

  useEffect(() => {
    initialize();
  }, [initialize]);

  // LOGIN
  const login = useCallback(async (email, password) => {
    const { data } = await axios.post('https://andrecenter.onrender.com/auth/login', {
      username: email,
      password,
    });

    const { username, phone, perms } = await setSession(data.access_token);

    dispatch({
      type: 'LOGIN',
      payload: {
        isAuthenticated: true,
        user: {
          name: username,
          email: phone,
          // eslint-disable-next-line no-nested-ternary
          role: perms === 0 ? 'Unverified' : perms === 1 ? 'verified' : 'admin',
        },
      },
    });
  }, []);

  // REGISTER
  const register = useCallback(async (username, password, phone) => {
    const { data } = await axios.get(
      `https://andrecenter.onrender.com/new/${username}/${password}/${phone}`
    );
    if (data) {
      return true;
    }
    return false;
  }, []);

  // LOGOUT
  const logout = useCallback(() => {
    // setSession(null);
    dispatch({
      type: 'LOGOUT',
    });
  }, []);

  const calls = useCallback(async (key) => {
    const { data } = await axios.post('https://andrecenter.onrender.com/call/all', { key });
    return data;
  }, []);

  const forwardCall = useCallback(
    async (number, key) => {
      const { data } = await axios.post(`https://andrecenter.onrender.com/call/forward/`, {
        key,
        number,
      });
      await calls();
      return data;
    },
    [calls]
  );

  const deleteCall = useCallback(async (number) => {
    const { data } = await axios.get(`https://andrecenter.onrender.com/call/delete/${number}`);
    return data;
  }, []);

  const accounts = useCallback(async (key) => {
    const { data } = await axios.post('https://andrecenter.onrender.com/auth/accounts', { key });
    return data;
  }, []);

  const deleteAccount = useCallback(async (key) => {
    const { data } = await axios.post('https://andrecenter.onrender.com/auth/accounts', { key });
    return data;
  }, []);

  const editAccount = useCallback(async ({ id, phone, perms, key }) => {
    console.log(id, phone, key, perms);
    const { data } = await axios.post('https://andrecenter.onrender.com/auth/update', {
      id,
      phone,
      perms,
      key,
    });
    return data;
  }, []);

  const memoizedValue = useMemo(
    () => ({
      isInitialized: state.isInitialized,
      isAuthenticated: state.isAuthenticated,
      user: state.user,
      method: 'jwt',
      login,
      register,
      logout,
      calls,
      forwardCall,
      deleteCall,
      accounts,
      deleteAccount,
      editAccount,
    }),
    [
      state.isAuthenticated,
      state.isInitialized,
      state.user,
      login,
      logout,
      register,
      calls,
      forwardCall,
      deleteCall,
      accounts,
      deleteAccount,
      editAccount,
    ]
  );

  return <AuthContext.Provider value={memoizedValue}>{children}</AuthContext.Provider>;
}
