import PropTypes from 'prop-types';
import { useContext, useState } from 'react';
// @mui
import {
  Stack,
  Avatar,
  Button,
  Checkbox,
  TableRow,
  MenuItem,
  TableCell,
  IconButton,
  Typography,
  Dialog,
  TextField,
  Grid,
} from '@mui/material';
import { useAuthContext } from '../../../../auth/useAuthContext';
// components
import Label from '../../../../components/label';
import Iconify from '../../../../components/iconify';
import MenuPopover from '../../../../components/menu-popover';
import ConfirmDialog from '../../../../components/confirm-dialog';

// ----------------------------------------------------------------------

UserTableRow2.propTypes = {
  row: PropTypes.object,
  selected: PropTypes.bool,
  forwardCall: PropTypes.func,
  onDeleteRow: PropTypes.func,
  onSelectRow: PropTypes.func,
};

export default function UserTableRow2({ row, selected, forwardCall, onSelectRow, onDeleteRow }) {
  const { id, phone, perms, username } = row;
  const { editAccount } = useAuthContext();

  const [openConfirm, setOpenConfirm] = useState(false);

  const [openPopover, setOpenPopover] = useState(null);

  const [openEditDialog, setOpenEditDialog] = useState(false);

  const key = localStorage.getItem('accessToken');

  const [values, setValues] = useState({
    phone: `${phone}`,
    perms: `${perms}`,
  });

  const handleOpenConfirm = () => {
    setOpenConfirm(true);
  };

  const handleCloseConfirm = () => {
    setOpenConfirm(false);
  };

  const handleOpenPopover = (event) => {
    setOpenPopover(event.currentTarget);
  };

  const handleClosePopover = () => {
    setOpenPopover(null);
  };

  return (
    <>
      <TableRow hover selected={selected}>
        <TableCell padding="checkbox">
          <Checkbox checked={selected} onClick={onSelectRow} />
        </TableCell>

        <TableCell>
          <Stack direction="row" alignItems="center" spacing={2}>
            <Typography variant="subtitle2" noWrap>
              {id}
            </Typography>
          </Stack>
        </TableCell>

        <TableCell align="left">{username}</TableCell>

        <TableCell align="left" sx={{ textTransform: 'capitalize' }}>
          {perms}
        </TableCell>

        <TableCell align="left">{phone}</TableCell>

        <TableCell align="right">
          <IconButton
            color={openPopover ? 'inherit' : 'default'}
            onClick={() => setOpenEditDialog(true)}
          >
            <Iconify icon="eva:edit-fill" />
          </IconButton>
          <IconButton color={openPopover ? 'inherit' : 'default'} onClick={handleOpenPopover}>
            <Iconify icon="eva:more-vertical-fill" />
          </IconButton>
        </TableCell>
      </TableRow>

      <MenuPopover
        open={openPopover}
        onClose={handleClosePopover}
        arrow="right-top"
        sx={{ width: 140 }}
      >
        <MenuItem
          onClick={() => {
            // handleEditMenu();
            handleClosePopover();
          }}
          sx={{ color: 'primary.main' }}
        >
          <Iconify icon="eva:edit-fill" />
          Edit
        </MenuItem>
        <MenuItem
          onClick={() => {
            handleOpenConfirm();
            handleClosePopover();
          }}
          sx={{ color: 'error.main' }}
        >
          <Iconify icon="eva:trash-2-outline" />
          Delete
        </MenuItem>
      </MenuPopover>

      <Dialog open={openEditDialog} onClose={() => setOpenEditDialog(false)}>
        <form
          onSubmit={(e) => {
            e.preventDefault();
            values.key = key;
            values.id = id;
            editAccount(values);
            setTimeout(() => {
              window.location.reload();
            }, 1000);
          }}
        >
          <Stack spacing={2} sx={{ padding: 5 }}>
            <TextField
              placeholder="Phone"
              label="Phone"
              value={values.phone}
              onChange={(e) => setValues((prev) => ({ ...prev, phone: e.target.value }))}
            />
            <TextField
              placeholder="permission"
              label="Permission"
              type="number"
              min={0}
              max={2}
              value={values.perms}
              onChange={(e) => setValues((prev) => ({ ...prev, perms: e.target.value }))}
            />
          </Stack>

          <Grid container justifyContent="space-around">
            <Button type="submit">Submit</Button>
            <Button onClick={() => setOpenEditDialog(false)}>Cancel</Button>
          </Grid>
        </form>
      </Dialog>

      <ConfirmDialog
        open={openConfirm}
        onClose={handleCloseConfirm}
        title="Delete"
        content="Are you sure want to delete?"
        action={
          <Button variant="contained" color="error" onClick={onDeleteRow}>
            Delete
          </Button>
        }
      />
    </>
  );
}
