// routes
import { PATH_DASHBOARD } from '../../../routes/paths';
// components
import SvgColor from '../../../components/svg-color';

// ----------------------------------------------------------------------

const icon = (name) => (
  <SvgColor src={`/assets/icons/navbar/${name}.svg`} sx={{ width: 1, height: 1 }} />
);

const ICONS = {
  user: icon('ic_user'),
  ecommerce: icon('ic_ecommerce'),
  analytics: icon('ic_analytics'),
  dashboard: icon('ic_dashboard'),
};

const navConfig = [
  // GENERAL
  // ----------------------------------------------------------------------
  {
    subheader: 'general v4.1.0',
    items: [
      { title: 'User', path: PATH_DASHBOARD.user, icon: ICONS.dashboard },
      { title: 'Calls', path: PATH_DASHBOARD.calls, icon: ICONS.ecommerce },
      {
        title: 'Admin',
        path: PATH_DASHBOARD.admin.users,
        icon: ICONS.analytics,
        children: [
          { title: 'Accounts', path: PATH_DASHBOARD.admin.users },
          { title: 'Audios', path: PATH_DASHBOARD.admin.audios },
        ],
      },
    ],
  },
];

export default navConfig;
