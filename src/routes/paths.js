// ----------------------------------------------------------------------

function path(root, sublink) {
  return `${root}${sublink}`;
}

const ROOTS_DASHBOARD = '/dashboard';

// ----------------------------------------------------------------------

export const PATH_AUTH = {
  login: '/login',
  register: '/register',
};

export const PATH_DASHBOARD = {
  root: ROOTS_DASHBOARD,
  user: path(ROOTS_DASHBOARD, '/user'),
  calls: path(ROOTS_DASHBOARD, '/calls'),

  admin: {
    root: path(ROOTS_DASHBOARD, '/admin'),
    users: path(ROOTS_DASHBOARD, '/admin/users-list'),
    audios: path(ROOTS_DASHBOARD, '/admin/audios'),
  },
};
